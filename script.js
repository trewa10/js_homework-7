/* Дякую за відгуки про попередні ДЗ! Дуже мотивує до навчання))

1. Опишіть своїми словами як працює метод forEach.
  Метод forEach приймає в якості аргумента callback функцію, по черзі перебирає кожне значення в масиві і застосовує до нього цю функцію.
  Також forEach передає в callback функцію аргументами чергове значення масиву, його індекс та сам масив.

2. Як очистити масив?
  Можна зробити довжину масиву 0 (array.length = 0), це мабуть найкоротший спосіб.
  Ще є спосіб використати метод splice (array.splice(0, array.length);

3. Як можна перевірити, що та чи інша змінна є масивом?
  Метод Array.isArray(), в який аргументом передати змінну. Повернеться true, якщо передана змінна є масивом, інакше false.
*/

"use strict";
// Написати функцію, яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом.

// Варіант 1 forEach
function filterBy(arr, type) {
  let newArr = [];
  arr.forEach(function (item) {
        if (typeof item !== type) {
          newArr.push(item);
        }
      });
   return newArr;
}

// Варіант 2 filter
function filterBy2(arr, type) {
  return arr.filter(item => typeof(item) !== type);
}

// Варіант 3 (forEach з перевіркою на null та object). Вийшло досить громіздко і не дуже читабельно, але воно наче працює))
function filterBy3(arr, type) {
  let newArr = [];
   if (type === "null") {
      newArr = arr.filter(item => item !== null);
   } else if (type === "object") {
      arr.forEach(function (item) {
        if (item === null) {
        newArr.push(item);  
       } else if (typeof item !== type) {
        newArr.push(item);
    }
  });  
   } else {
     arr.forEach(function (item) {
    if (typeof item !== type) {
      newArr.push(item);
    }
  });
   }
  return newArr;
}

// тестовий масив даних
const testArray = ["hello", "world", 23, "23", NaN, null, undefined, [1, "Alex", true], false, null, 12875545n, {}];

const forEachArray = filterBy(testArray, "number");
console.log(forEachArray);

const filterArray = filterBy2(testArray, "string");
console.log(filterArray);

const forEachNullArray = filterBy3(testArray, "oject");
console.log(forEachNullArray);